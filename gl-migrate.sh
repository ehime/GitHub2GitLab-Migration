#!/usr/local/bin/bash
# GTFO yo

# CPR : Jd Daniel :: Ehime-ken
# MOD : 2018-06-04 @ 10:58:38
# VER : Version 1.0.0a

# USE : ./gl-migrate.sh -hu $GH_USER -lu $GL_USER -ht $GH_USER_TOKEN -lt $GL_USER_TOKEN
# REQ : Bash 4.3+

################################################################################
################################################################################

declare REPOS=''

# # trap ctrl-c and call ctrl_c()
# trap ctrl_c INT
#
# tmpdir=$(mktemp -d 2>/dev/null || mktemp -d -t 'tmpdir')
# function ctrl_c() {
#   echo "** Trapped abort, cleaning up.."
#   rm -fr "${tmpdir}"
# }

function print_usage()
{
  echo -e '
  Parameter usage: gl-migrate.sh [--help] [--hub_user=${hub_user}] [--lab_user=${lab_user}] [--hub_user=${hub_user}] [--lab_user=${lab_user}]

  Parameters:
  -hu  --hub_user   Github organisation conencting to
  -lu  --lab_user   File to locate in repositories
  -ht  --hub_token  Search term to match in file
  -lt  --lab_token  Github username for API connection

  Example usage:
  gh-search.sh --hub_user ehime --lab_user ehimeprefecture  \
    --hub_token   abcdef123456789                           \
    --lab_token   zyxqvu987654321                           \
    --preserve_visibility
  '
}


## Following requires modern GNU bash 3.2 WILL FAIL....
if (shopt -s nocasematch ; [[ $1 = @(-h|--help) ]]) ; then
  print_usage ; exit 1
else
  while [[ $# -gt 0 ]] ; do
    opt="${1}" ; shift
    current_arg="${1}"

    if [[ "$current_arg" =~ ^-{1,2}.* ]] ; then
      echo "=> You may have left an argument blank. Double check your command."
    fi
    case "${opt}" in
      "-hu"|"--hub_user"            ) hu="${1}"                                     ; shift  ;;
      "-lu"|"--lab_user"            ) lu="${1}"                                     ; shift  ;;
      "-ht"|"--hub_token"           ) ht="${1}"                                     ; shift  ;;
      "-lt"|"--lab_token"           ) lt="${1}"                                     ; shift  ;;
      "-p" |"--preserve_visibility" ) prv="${1}"                                    ; shift  ;;
      *                             ) echo "ERROR: Invalid option: \""$opt"\"" >&2  ; exit 1 ;;
    esac
  done
fi


## gewaltenteilung
[[ -z "$hu" ]]  && { echo "=> [GitHub User]  (-hu|--hub_user)  cannot remain empty" >&2 ; exit 1 ; }
[[ -z "$lu" ]]  && { echo "=> [GitLab User]  (-lu|--lab_user)  cannot remain empty" >&2 ; exit 1 ; }
[[ -z "$ht" ]]  && { echo "=> [GitHub Token] (-ht|--hub_token) cannot remain empty" >&2 ; exit 1 ; }
[[ -z "$lt" ]]  && { echo "=> [GitLab Token] (-lt|--lab_token) cannot remain empty" >&2 ; exit 1 ; }


## Set CURLs for different orgs
GH_CURL="
curl --silent
  -H \"Accept: application/vnd.github.v3+json\"
  -H \"Authorization: token ${ht}\"
"

GL_CURL="
curl --silent
  -H \"Content-Type:application/json\"
  -H \"Private-Token: ${lt}\"
"

GH_TOTAL=$(eval $GH_CURL -I -G https://api.github.com/users/${hu}/repos         \
|grep 'Link: ' |sed -e 's/, /\n/g' |grep -Po '.*page=\K[0-7]{1,}.*last' |head -c1)


echo -e "[info] Gatherign repositories for ${gu}"
for ITER in $(seq 1 ${GH_TOTAL}) ; do

  for REPO in $(eval $GH_CURL --data-urlencode "'q=org:${org} in:path ${file}'"  \
    --data-urlencode "'per_page=100'"                                         \
    --data-urlencode "'page=${ITER}'"                                         \
    -G https://api.github.com/search/code |jq -r '.items[].repository.name') ; do

    REPOS+=($REPO) ##will de-dupe so you'll get a diff # than total

  done
done
#echo $GL_CURL --data-urlencode "'username=${lu}'" -G https://gitlab.com/api/v4/users
